#!/bin/bash
rsync -avz $HOME/.bashrc .bashrc
rsync -avz $HOME/.config/nvim/init.vim .config/nvim/init.vim
rsync -avz $HOME/.tmux.conf .tmux.conf
